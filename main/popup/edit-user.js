//// Lets the user view and edit a User.




//// DEPENDENCIES

import Popup from '../class/popup.js'
import hub from '../core/hub.js'




//// CLASS AND SINGLETON

class EditUser extends Popup {

    //// Returns an object containing this module’s Vue methods.
    get vueMethods () {
        return Object.assign({}, super.vueMethods, {
            deleteItem () { hub.fire('user-delete', this.item.id) }
        })
    }

    //// Returns the inside of this module’s Vue template-string.
    get vueTemplateInner () { let innerHTML; return innerHTML = `
    <h1>Edit User</h1>
    <div v-if="'edit-user' === Router.popupAction">
      <oomod-user-form
        v-if="UserList.items.find( el => Router.popupArgs[0] == el.id )"
        v-bind:item="UserList.items.find( el => Router.popupArgs[0] == el.id )">
      </oomod-user-form>
    <div v-else>No such user #{{ Router.popupArgs[0] }}</div>
    </div>
    <div v-else>Not editing a user</div>
    <a href="#list-users">List Users</a>
    `}

}

//// Create the singleton instance of this module.
new EditUser()
