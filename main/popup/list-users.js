//// Lets the user view and edit a list of Users.




//// DEPENDENCIES

import Popup from '../class/popup.js'
import hub from '../core/hub.js'




//// CLASS AND SINGLETON

class ListUsers extends Popup {

    //// Returns an object containing this module’s Vue methods.
    get vueMethods () {
        return Object.assign({}, super.vueMethods, {
            addItem () {
                const id = hub.fire('user-add').find( el => /^\d{7}$/.test(el) )
                if (! id) throw Error('Can’t find newly created User id')
                hub.fire('router-update-hash', 'edit-user/'+id)
            }
        })
    }

    //// Returns the inside of this module’s Vue template-string.
    get vueTemplateInner () { let innerHTML; return innerHTML = `
    <h1>Users</h1>
    <button v-on:click="addItem()">Add User</button>
    <oomod-user-list></oomod-user-list>
    `}

}

//// Create the singleton instance of this module.
new ListUsers()
