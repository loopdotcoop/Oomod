//// Usage example - how to define a typical item.




//// DEPENDENCIES

import Item from '../class/item.js'
import config from '../core/config.js'
import hub from '../core/hub.js'
const IS_EDITABLE = 'IS_EDITABLE'




//// RECORD API

hub.on('all-modules-booted', e => {

    //// Create this item’s shared config object and record its `api`.
    if (! config.Thing || ! config.Thing.api)
        hub.fire('config-set', Thing.name, 'api', Thing.api)

})




//// CLASS

export default class Thing extends Item {




    //// API

    static get api () {
        return Object.assign({}, super.api, {
            flag: {
                default: false
              , IS_EDITABLE
            }
        })
    }




    //// CONSTRUCTOR

    constructor (custom={}) {
        super(custom)

        //// Record custom instantiation values, or fall back to defaults.
        const { flag } = custom
        const api = this.constructor.api
        this.flag = null == flag ? api.flag.default : flag

    }

}
