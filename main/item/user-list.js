//// A list of Users.




//// DEPENDENCIES

import List from '../class/list.js'
import state from '../core/state.js'
import hub from '../core/hub.js'
import User from './user.js' // for itemClass, but also to record its API
import UserRow from './user-row.js' // for rowClass




//// CLASS

class UserList extends List {

    //// An instance of this class will be passed to 'state-add'.
    get itemClass () { return User }

    //// Useful for building vueTemplateInner.
    get rowClass () { return UserRow }

    //// Xx.
    constructor () {
        super()

        hub.on('user-edit', (evtName, id, prop, val) => {
            const item = state.UserList.items.find( item => id === item.id )
            const isInvalid = ! this.itemClass.validate(item)
            hub.fire('state-edit', 'UserList', 'items', id, 'isInvalid', isInvalid )
        })
    }

}

//// Create the singleton instance of this module.
new UserList()
