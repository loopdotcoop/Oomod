//// Base class for all form modules, eg item/UserForm.js.




//// DEPENDENCIES

import Module from './module.js'
import config from '../core/config.js'
import hub from '../core/hub.js'




//// CLASS

export default class Form extends Module {

    //// Make `config` available to the form, so that it can read item APIs.
    get vueComputed () {
        return Object.assign({}, super.vueComputed, {
            config () { return config }
        })
    }

    //// Returns an object containing this form’s Vue methods.
    get vueMethods () {
        return Object.assign({}, super.vueMethods, {
            api (item, fieldName) { return config[item.className].api[fieldName] }
          , editField (fieldName) { //@TODO guard against uneditable field?
                hub.fire(this.item.tagName+'-edit', this.item.id, fieldName, this.item[fieldName])
            }
          , fieldIsInvalid () {
                return false
            }
        })
    }

    //// Add 'oomod-form' and 'invalid' classes to the template-string.
    get vueTemplate () { return `
        <div
          class="oomod-form ${this.vueTagName}"
          v-bind:class="{ invalid:item.isInvalid }"
        >${this.vueTemplateInner}
        </div>`
    }

    //// Returns the inside of this module’s Vue template-string.
    get vueTemplateInner () { let innerHTML; return innerHTML = `
    <label
      v-for="fieldName in Object.keys(item)"
      v-if="api(item,fieldName).IS_ON_FORM"
      v-bind:class="{ invalid:fieldIsInvalid() }"
    >{{ fieldName }}
      <input
        v-bind:disabled="! config[item.className].api[fieldName].IS_EDITABLE"
        v-model="item[fieldName]"
        v-on:change="editField(fieldName)">
    </label>
    `}

}
