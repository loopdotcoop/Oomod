//// Base class for all of the app’s rows, eg item/FileRow.js.




//// DEPENDENCIES

import Module from './module.js'
import config from '../core/config.js'
import hub from '../core/hub.js'




//// CLASS

export default class Row extends Module {

    //// Make `config` available to the row, so that it can read item APIs.
    get vueComputed () {
        return Object.assign({}, super.vueComputed, {
            config () { return config }
        })
    }

    //// Returns an object containing this row’s Vue methods.
    get vueMethods () {
        return Object.assign({}, super.vueMethods, {
            editField (fieldName) { //@TODO guard against uneditable field?
                hub.fire(this.item.tagName+'-edit', this.item.id, fieldName, this.item[fieldName])
            }
          , deleteItem () {
                hub.fire(this.item.tagName+'-delete', this.item.id)
            }
          , editItem () {
                hub.fire('router-update-hash', `edit-${this.item.tagName}/${this.item.id}`)
            }
        })
    }

    //// Add 'oomod-row' and 'invalid' classes to the template-string.
    get vueTemplate () { return `
        <div
          class="oomod-row ${this.vueTagName}"
          v-bind:class="{ invalid:item.isInvalid }"
        >${this.vueTemplateInner}
        </div>`
    }

    //// Returns the inside of this module’s Vue template-string.
    get vueTemplateInner () { let innerHTML; return innerHTML = `
    <input
      v-for="fieldName in Object.keys(item)"
      v-if="config[item.className].api[fieldName].IS_ON_ROW"
      v-bind:disabled="! config[item.className].api[fieldName].IS_EDITABLE"
      v-model="item[fieldName]"
      v-on:change="editField(fieldName)"
    >
    <button v-on:click="deleteItem">Delete</button>
    <button v-on:click="editItem">Edit</button>
    `}

}
