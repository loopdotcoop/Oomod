//// Base class for the app’s popup modules, eg popup/welcome.js.




//// DEPENDENCIES

import Singleton from './singleton.js'
import hub from '../core/hub.js'




//// CLASS

export default class Popup extends Singleton {

    //// All popups have a closePopup() method.
    get vueMethods () {
        return Object.assign({}, super.vueMethods, {
            closePopup () { hub.fire('router-close-popup') }
        })
    }

    //// HTML container for this core module’s Vue instance.
    get $vueWrap () { return document.querySelector('#oomod-popup') }

    //// Same as a Module template-string, with a ‘close’ button.
    get vueTemplate () { let innerHTML; return innerHTML = `
    <div
      class="${this.vueTagName}"
      v-bind:class="{ show:'${this.tagName}' === Router.popupAction }">
      <div>
        <button v-on:click="closePopup" class="close" title="Close">&times;</button>
        ${this.vueTemplateInner}
      </div>
    </div>
    `}

}
