//// Base class for the app’s grid modules, eg popup/meta.js.




//// DEPENDENCIES

import Singleton from './singleton.js'
import hub from '../core/hub.js'




//// CLASS

export default class Grid extends Singleton {

    //// All grid modules have gridFillOne() and gridShowAll() methods.
    get vueMethods () {
        return Object.assign({}, super.vueMethods, {
            gridFillOne () { hub.fire('router-grid-fill-one', this.class.tagName ) }
          , gridShowAll () { hub.fire('router-grid-show-all' ) }
        })
    }

    //// HTML container for this core module’s Vue instance.
    get $vueWrap () { return document.querySelector('#oomod-grid') }

    //// Xx.
    get vueTemplate () { let innerHTML; return innerHTML = `
    <div
      class="${this.vueTagName}"
      v-bind:class="{
          fill: '${this.tagName}' === Router.gridAction
        , show: 'project' === Router.gridAction
      }">
      <div>
        <button v-on:click="gridFillOne" class="maximize" title="Maximize">+</button>
        <button v-on:click="gridShowAll" class="minimize" title="Minimize">-</button>
        ${this.vueTemplateInner}
      </div>
    </div>
    `}

}
