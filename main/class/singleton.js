//// Base class for the app’s core, grid and popup modules, eg core/config.js.




//// DEPENDENCIES

import Module from './module.js'
import hub from '../core/hub.js'




//// CLASS

export default class Singleton extends Module {

    //// HTML container for this core module’s Vue instance.
    get $vueWrap () { return document.querySelector('body') }

    //// Core, grid and popup modules each have a single Vue element, which is
    //// appended to the DOM during the boot process. The 'app-ready' event will
    //// not fire until all of these have mounted.
    get vueMounted () {
        const name = this.constructor.name
        return function () {

            //// Update the `mounted` state, and signal that the Vue instance
            //// has mounted. Modules which override this `vueMounted` property
            //// must still fire both events.
            hub.fire('state-set', name, 'mounted', 'ok')
            hub.fire('vue-instance-mounted', name)
            //// @TODO have its child-elements mounted?
        }
    }

    //// Returns this module’s global CSS, which will be injected into the
    //// <style id="oomod-style"> element.
    // get globalStyle () { return '' }

    ////
    constructor () {
        super()

        //// Inject this module’s global CSS into the DOM.
        // document.querySelector('#oomod-style').innerHTML += this.globalStyle

        //// Initialise the `mounted` state.
        hub.fire('state-set', this.constructor.name, 'mounted', 'not-yet')
    }

}
